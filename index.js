/**
 * Created by victor.oliveira on 18/02/2016.
 */

    angular.module('demoApp', ['ngMaterial', 'ngAnimate', 'ctt.directives'])

    .controller('demoCtrl', ['$scope', '$q', '$timeout', function($scope, $q, $timeout) {

        $scope.pivotGrid = {
            dataSource: []
        };

        $scope.decorateRowLabel = function(label, additionalInfo) {

            if (label !== additionalInfo){
                return label + "/" + additionalInfo;
            }

            switch (label) {
                case "2":
                    return "Fevereiro";
                case "4":
                    return "Abril";
                default:
                    return "(Sem Titulo)";
            }
        };

        $scope.stylizeCell = function(item) {

            if (_.indexOf(item.properties, "WR") > -1)
            {
                return 'border-top: 5px solid darkgrey !important;';
            }

            return '';
        };

        $scope.dblClick = function(ev, item){
            console.info(ev, item);
        };

        $scope.sort = function(a,b) { return a-b; };

        var promiseValues = $q.when([
            {
                day: 1,
                month: 4,
                monthReference: 3,
                label: "F",
                desc: "Folga<br>blablablablablab",
                color: "#00aaff",
                properties : ["WR"]
            },
            {
                day: 2,
                month: 2,
                monthReference: 2,
                label: "E",
                desc: "Embarque",
                color: "#ffaa00",
                properties : ["WR"]
            },
            {
                day: 3,
                month: 2,
                monthReference: 2,
                label: "F",
                desc: "Folga",
                color: "#00aaff",
                properties : ["WR"]
            },
            {
                day: 4,
                month: 2,
                monthReference: 2,
                label: "F",
                desc: "Folga",
                color: "#00aaff"
            },
            {
                day: 5,
                month: 2,
                monthReference: 2,
                label: "F",
                desc: "Folga",
                color: "#00aaff"
            },
            {
                day: 6,
                month: 2,
                monthReference: 2,
                label: "F",
                desc: "Folga",
                color: "#00aaff"
            }
        ]);

        $timeout(resolvePromiseValues, 300);

        function resolvePromiseValues() {
            promiseValues.then(function(data){
                $scope.pivotGrid.dataSource = data;
            });
        }

        $scope.htmlContent = "<!DOCTYPE html><html>    <head>        <title>Opa</title>    </head>    <body>        <a href=\"javascript:alert(\'teste\');\">Link</a>    </body></html>";

        $scope.table = {
            "GlobalParameters": [
                {
                    "Name": "Logo",
                    "Value": ""
                },
                {
                    "Name": "Empresa",
                    "Value": ""
                }
            ],
            "QueryResult": {
                "Columns": [
                    {
                        "DataType": "System.String",
                        "Hidden": false,
                        "IsId": false,
                        "Name": "Nome do Usuário",
                        "Position": 1,
                        "Text": "Nome do Usuário",
                        "Format": null
                    },
                    {
                        "DataType": "System.Boolean",
                        "Hidden": false,
                        "IsId": false,
                        "Name": "Ativo",
                        "Position": 1,
                        "Text": "Ativo",
                        "Format": null
                    }
                ],
                "Rows": [
                    [
                        "Administrador",
                        true
                    ],
                    [
                        "adriana.correa",
                        null
                    ],
                    [
                        "adriana.moraes",
                        null
                    ],
                    [
                        "alana.leite",
                        null
                    ],
                    [
                        "alessandra.telles",
                        null
                    ],
                    [
                        "alex.silva",
                        null
                    ],
                    [
                        "alex.sodre",
                        null
                    ],
                    [
                        "alexsandro.siedschlag",
                        null
                    ],
                    [
                        "alice.bravo",
                        null
                    ],
                    [
                        "alvaro.santos",
                        null
                    ],
                    [
                        "amanda.oliveira",
                        null
                    ],
                    [
                        "ana.braga",
                        null
                    ],
                    [
                        "ANA.COSTA",
                        null
                    ],
                    [
                        "anderson.bernardes",
                        null
                    ],
                    [
                        "ANDRE.COSTA",
                        null
                    ]
                ],
                "ExecutionParams": [],
                "QueryId": "72b3a3d7-ba71-4285-bff9-a8a1bb24a9c4",
                "QueryCode": "2",
                "QueryName": "Usuários 2",
                "QueryDescription": "Lista de usuários do Drake, sem parâmetros!",
                "End": 15,
                "Start": 1,
                "PageNumber": 1,
                "TotalPages": 8,
                "TotalRows": 117
            }
        };

    }]);