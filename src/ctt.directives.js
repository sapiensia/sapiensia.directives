/**
 * Created by victor.oliveira on 18/02/2016.
 */

(function(angular, _){
    'use strict';

    if (typeof(angular) == 'undefined' || typeof(_) == 'undefined') {
        throw new Error('angular and lodash are needed');
    }

    angular.module('ctt.directives', [])
        .directive('cttOnEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keyup", function (event) {
                    if(event.which === 13) {
                        scope.$eval(attrs.cttOnEnter);
                        event.preventDefault();
                    }
                });
            }
        })
        .directive('cttResize', ['$window', function($window){

            function getHeight(w){
                if(typeof w.height === 'function'){
                    return w.height();
                }

                return w.innerHeight;
            }

            return {
                restrict: 'A',
                scope: {
                    fixHeight: '@'
                },
                link: function (scope, element) {

                    var w = angular.element($window);
                    var changeHeight = function () { element.css('height', (getHeight(w) - scope.fixHeight) + 'px'); };

                    w.bind('resize', function () {
                        changeHeight();   // when window size gets changed
                    });

                    changeHeight(); // when page loads

                    scope.$watch('fixHeight', function () {
                        changeHeight();
                    });
                }
            };
        }])
        .directive('cttPivotGrid', ['$compile', function($compile){

            return {
                restrict: 'E',
                required: '^ngModel',
                replace: true,
                scope: {
                    rowPropertyBind: '=',
                    rowAdditionalInfoPropertyBind: '=',
                    rowLabelDecorateFn: '=',
                    columnPropertyBind: '=',
                    columnInitValue: '=',
                    columnOrderFn: '&',
                    columnLabelDecorateFn: '=',
                    cellStyleFn: '=',
                    cellTextPropertyBind: '=',
                    cellTooltipPropertyBind: '=',
                    cellBgColorPropertyBind: '=',
                    dblClickFn: '=',
                    borderless: '=',
                    dataSource: '=ngModel'
                },
                template: '<table id="cttPivotGrid" class="ctt ctt-pivot-grid"></table>',

                controller: ['$scope', function($scope){

                    $scope.items = {};

                    $scope.generateDataColumns = function(columns, values, row) {

                        var html = '';
                        var organizedColumns = organizeLabelColumns(columns);

                        if(angular.isArray(organizedColumns) && organizedColumns.length > 0) {

                            for (var i = 0; i < organizedColumns.length; i++) {

                                var item = _.find(values, function (obj) { return obj[$scope.columnPropertyBind] == organizedColumns[i] });

                                if (item) {

                                    $scope.items['_' + row + '_' + organizedColumns[i] + '_'] = item;

                                    var style = '';

                                    if (angular.isDefined($scope.cellStyleFn) && angular.isFunction($scope.cellStyleFn)) {
                                        style = $scope.cellStyleFn(item);
                                    }

                                    if (angular.isDefined($scope.dblClickFn) && angular.isFunction($scope.dblClickFn)) {

                                        var key = 'items["_' + row + '_' + organizedColumns[i] + '_"]';
                                        html += '<td class="ctt-column-item" ng-dblclick=\'dblClickFn($event, ' + key + ');\' ng-class="{ borderless : borderless }" style="background-color: ' + item[$scope.cellBgColorPropertyBind] + ';' + style + '">';

                                    } else {
                                        html += '<td class="ctt-column-item" ng-class="{ borderless : borderless }" style="background-color: ' + item[$scope.cellBgColorPropertyBind] + ';' + style + '">';
                                    }

                                    html += '<span>' + item[$scope.cellTextPropertyBind] + '</span>';
                                    html += '<div class="ctt-tooltip">' + item[$scope.cellTooltipPropertyBind] + '</div>';
                                    html += '</td>';

                                } else {
                                    html += '<td class="ctt-column-item" ng-class="{ borderless : borderless }"><span>&nbsp;</span></td>';
                                }
                            }
                        }

                        return html;
                    };

                    $scope.generateLabelColumns = function(columns) {

                        var html = '<tr class="ctt-row-column-labels"><td class="ctt-column-empty"></td>';

                        var organizedColumns = organizeLabelColumns(columns);

                        if(angular.isArray(organizedColumns) && organizedColumns.length > 0) {

                            for (var i = 0; i < organizedColumns.length; i++){

                                if (angular.isDefined($scope.columnLabelDecorateFn) && angular.isFunction($scope.columnLabelDecorateFn)){
                                    html += '<td class="ctt-column-item" ng-class="{ borderless : borderless }"><span>' + $scope.columnLabelDecorateFn(organizedColumns[i]) + '</span></td>';
                                } else {
                                    html += '<td class="ctt-column-item" ng-class="{ borderless : borderless }"><span>' + organizedColumns[i] + '</span></td>';
                                }
                            }
                        }

                        html += '</tr>';
                        return html;
                    };

                    function organizeLabelColumns(columns) {

                        var values = [];

                        if (angular.isFunction($scope.columnOrderFn())){
                            values = _.keysIn(columns).sort($scope.columnOrderFn());
                        } else {
                            values = _.keysIn(columns).sort();
                        }

                        var exist = _.some(values, function(value){ return $scope.columnInitValue == value });

                        if (exist) {
                            for (var i = 0; i < values.length; i++){
                                if ($scope.columnInitValue != values[0]){
                                    var actual = values[0];
                                    values.shift();
                                    values.push(actual);
                                } else {
                                    break;
                                }

                            }
                        }

                        return values;
                    }

                }],
                link: function (scope, el) {

                    scope.$watch('dataSource', function() {

                        if (angular.isArray(scope.dataSource)) {

                            var groupedColumns = _.groupBy(scope.dataSource, scope.columnPropertyBind);
                            var groupedRows = _.groupBy(scope.dataSource, scope.rowPropertyBind);

                            var html = scope.generateLabelColumns(groupedColumns);

                            for (var key in groupedRows) {

                                var rowColumnsInfo = groupedRows[key];

                                var additionalValues = _.keysIn(_.groupBy(_.map(rowColumnsInfo, function(obj){
                                    return obj[scope.rowAdditionalInfoPropertyBind];
                                })));

                                if (angular.isDefined(scope.rowLabelDecorateFn) && angular.isFunction(scope.rowLabelDecorateFn)){
                                    html += '<tr class="ctt-row-item"><td class="ctt-row-label" ng-class="{ borderless: borderless }">' + scope.rowLabelDecorateFn(key, additionalValues) + '</td>';
                                } else {
                                    html += '<tr class="ctt-row-item"><td class="ctt-row-label" ng-class="{ borderless: borderless }">' + key + '</td>';
                                }

                                html += scope.generateDataColumns(groupedColumns, rowColumnsInfo, rowColumnsInfo[0][scope.rowPropertyBind]);
                                html += '</tr>';

                            }

                            el.html(html);
                            $compile(el.contents())(scope);

                        }

                    });
                }
            }

        }])
        .directive('cttIframeContentInjector', function(){

            return {
                restrict: 'A',
                scope: {
                    content: '@'
                },
                link: function (scope, el) {

                    if (el[0].nodeName.toLowerCase() !== 'iframe'){
                        throw new Error('Element must be an IFRAME');
                    }

                    var iframe = el[0];
                    var html_string = '';
                    var iframedoc = getIframeDoc();

                    scope.$watch('content', function(newValue) {

                        html_string = newValue;

                        if (iframedoc){
                            iframedoc.open();
                            iframedoc.writeln(html_string);
                            iframedoc.close();
                        } else {
                            console.error('Cannot inject dynamic contents into iframe.');
                        }

                    });

                    function getIframeDoc() {
                        if (iframe.contentDocument){
                            return iframe.contentDocument;
                        } else if (iframe.contentWindow) {
                            return iframe.contentWindow.document;
                        }
                        return iframe.document;
                    }

                }
            };

        })
        .directive('cttTable', ['$compile', function($compile){

            return {
                restrict: 'E',
                replace: true,
                scope: {
                    columns: '=',
                    rows: '='
                },
                link: function (scope, el) {

                    scope.$watchGroup(['columns', 'rows'], function() {

                        var columns = scope.columns;
                        var rows = scope.rows;

                        var html = '<table class="ctt ctt-table">';

                        html += '<thead>';
                        html += '<tr>';

                        for (var c = 0; c < columns.length; c++) {
                            html += '<th>' + columns[c].Text + '</th>';
                        }

                        html += '</tr>';
                        html += '</thead>';

                        html += '<tbody>';

                        for (var i = 0; i < rows.length; i++ ) {
                            html += '<tr>';
                            for (var index in rows[i]) {
                                if (index < columns.length){
                                    html += '<td>' + rows[i][index] + '</td>';
                                }
                            }
                            html += '</tr>';
                        }

                        html += '</tbody>';
                        html += '</table>';

                        el.html(html);
                        $compile(el.contents())(scope);
                    });
                }
            };

        }]);

}(angular, (_||lodash)));