module.exports = function(grunt) {

  grunt.initConfig({
      clean: {
          cache: ['.sass-cache']
      },
      sass: {
          build: {
              files: {
                  'src/styles/ctt.directives.css': 'src/styles/ctt.directives.scss'
              }
          }
      },
      cssmin: {
          build: {
              files: {
                  'src/styles/ctt.directives.min.css': ['src/styles/ctt.directives.css']
              }
          }
      },
      uglify: {
          build: {
              files: {
                  'src/ctt.directives.min.js': ['src/ctt.directives.js']
              }
          }
      },
      bump: {
          options: {
              files: ['package.json', 'bower.json'],
              updateConfigs: [],
              commit: true,
              commitMessage: 'Release v%VERSION%',
              commitFiles: ['package.json', 'bower.json'],
              createTag: true,
              tagName: 'v%VERSION%',
              tagMessage: 'Version %VERSION%',
              push: true,
              pushTo: 'https://bitbucket.org/sapiensia/sapiensia.directives.git',
              gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
              globalReplace: false,
              prereleaseName: false,
              metadata: '',
              regExp: false
          }
      }
  });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-bump');

    grunt.registerTask('build', ['clean', 'sass', 'cssmin', 'uglify', 'clean:cache']);
    grunt.registerTask('update', ['bump']);
};